import Home from "./home.md";
import HowToImplementSila from "./How-to-implement-SiLA.md";
import BrowserQuickstart from "./SiLA-Browser-Quickstart.md";
import Troubleshooting from "./Troubleshooting.md";
import WhatIsSila from "./What-is-SiLA.md";

export default [
    {
        source: Home,
        path: "/wiki/home",
    },
    {
        source: HowToImplementSila,
        path: "/wiki/how-to-implement-sila",
    },
    {
        source: BrowserQuickstart,
        path: "/wiki/sila-browser-quickstart",
    },
    {
        source: Troubleshooting,
        path: "/wiki/troubleshooting",
    },
    {
        source: WhatIsSila,
        path: "/wiki/what-is-sila",
    },
];
