# Welcome to SiLA 2

SiLA’s mission is to establish international standards which create open connectivity in lab automation.
SiLA’s vision is to create interoperability, flexibility and resource optimization for laboratory instruments
integration and software services based on standardized communication protocols and content specifications.
SiLA promotes open standards to allow integration and exchange of intelligent systems in a cost effective way.

## SiLA 2 Core Features

These are the current SiLA 2 core features, for more features, please select '_Related Pages_'

[AuthenticationService](feature_definitions/org/silastandard/core/AuthenticationService.sila.xml)

[AuthorizationService](feature_definitions/org/silastandard/core/AuthorizationService.sila.xml)

[CancelController](feature_definitions/org/silastandard/core/commands/CancelController.sila.xml)

[LockController](feature_definitions/org/silastandard/core/LockController.sila.xml)

[ParameterConstraintsProvider](feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider.sila.xml)

[SiLAService](feature_definitions/org/silastandard/core/SiLAService.sila.xml)

[SimulationController](feature_definitions/org/silastandard/core/SimulationController.sila.xml)

## [SiLA 2 Wiki]

[Wiki Home](/wiki/home)

[How to implement SiLA](/wiki/how-to-implement-sila)

[SiLA Browser quickstart](/wiki/sila-browser-quickstart)

[Troubleshooting](/wiki/troubleshooting)

[What is SiLA?](/wiki/what-is-sila)